package com.causecode.storeapp.app.resources;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.causecode.storeapp.app.model.Store;
import com.causecode.storeapp.app.services.StoreService;

@Path("/stores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StoreResources {

	StoreService service=new StoreService();
	List<Store> str; 
	@GET
	public List<Store> getAllStores()
	{
		List<Store> stores=service.getAllStores();
		str=new ArrayList<Store>();
		Iterator<Store> iterator=stores.iterator();
		while(iterator.hasNext())
		{
			Store store=iterator.next();
			System.out.println(store.getStoreName());
			str.add(store);
		}
		return  str;
	}
	
	@GET
	@Path("/{zipcode}")
	public List<Store> getStoreByFilter(@PathParam("zipcode")long zipCode,@PathParam("distance") int miles)
	{
		return new StoreService().getStoresByZip(zipCode, miles);
	}
	/*
	@PUT
	public String createStore(Store store)
	{
		new StoreService().createStore(store);
		return "Store has been added";
	}
	@POST
	public String updateStroe(Store store,@PathParam("storeId") long storeId)
	{
		new StoreService().updateStore(store, storeId);
		return "Store has been updated";
	}
	@DELETE
	public String deleteStore(@PathParam("storeId") long storeId)
	{
		try{
		new StoreService().deleteStore(storeId);
		return "Record has been deleted";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Upable to delete record";
	}
	*/
}
