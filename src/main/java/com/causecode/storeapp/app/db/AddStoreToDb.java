package com.causecode.storeapp.app.db;

import org.hibernate.Session;

import com.causecode.storeapp.app.model.Store;
import com.causecode.storeapp.app.utility.MyConnectionConfig;

public class AddStoreToDb {
	public Boolean addToDb(Store store) {
		Session session = MyConnectionConfig.getSession();
		Integer id = null;
		try {
			session.beginTransaction();
			id = (Integer) session.save(store);
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		if (id == null) {
			return false;
		} else {
			return true;
		}
	}
}
