
package com.causecode.storeapp.app.services;

import java.util.List;

import com.causecode.storeapp.app.db.AddStoreToDb;
import com.causecode.storeapp.app.db.GetStores;
import com.causecode.storeapp.app.model.Store;

public class StoreService {
	public Store getStoreById(long id)
	{
		return new GetStores().getStoreById(id);
	}
	public List<Store> getStoresByZip(long zipCode,int miles)
	{
		return new GetStores().getStoreByFilter(zipCode, miles);
	}
	public List<Store> getAllStores()
	{
		return new GetStores().getAllStore();
	}
	public Boolean createStore(Store store) {
		new AddStoreToDb().addToDb(store);
		return true;
	}
	public Boolean updateStore(Store store,long storeId)
	{
		return true;
	}
	public Boolean deleteStore(long storeId)
	{
		return true;
	}
}
