package com.causecode.storeapp.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private long storeId;
	private String storeName;
	
	@OneToOne (cascade= CascadeType.ALL)
	@JoinTable(name="Address")@JoinColumn(name="addressId")
	private Address storeAddress;
	public long getStoreId() {
		return storeId;
	}
	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Address getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(Address storeAddress) {
		this.storeAddress = storeAddress;
	}
}
