package com.causecode.storeapp.app.utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MyConnectionConfig {

	private static SessionFactory factory = null;
	private static Session session = null;

	private MyConnectionConfig() {
	}

	@SuppressWarnings("deprecation")
	public static Session getSession() {
		if (session == null) {
			try {
				factory = new Configuration().configure().buildSessionFactory();
				session = factory.openSession();
			} catch (Throwable ex) {
				System.err.println("Failed to create sessionFactory object." + ex);
				throw new ExceptionInInitializerError(ex);
			}
			return session;
		} else {
			if(session.isOpen())
			return session;
			else
			{
				session=factory.openSession();
				return session;
			}
		}

	}
}
