package com.causecode.storeapp.app.db;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.causecode.storeapp.app.model.Store;
import com.causecode.storeapp.app.utility.MyConnectionConfig;

public class GetStores {

	List<Store> stores = null;
	
	@SuppressWarnings("unchecked")
	public List<Store> getAllStore() {
		stores = new ArrayList<Store>();
		Session session = MyConnectionConfig.getSession();
		stores = session.createQuery("From Store").list();
		session.close();
		return stores;	
	}

	public Store getStoreById(long id) {
		Session session = MyConnectionConfig.getSession();
		Store store = (Store) session.createQuery("Store where storeId =:id").setParameter("id", id).list();
		session.close();
		return store;
	}
	
	@SuppressWarnings("unchecked")
	public List<Store> getStoreByFilter(long zipCode,int miles)
	{
		Session session= MyConnectionConfig.getSession();
		stores = session.createQuery("Store where storeZipCode=:zipCode and distance:distance").setParameter(0, zipCode).setParameter(1, miles).list();
		session.close();
		return stores;
	}

}
